package duckhouse;

/**
 * This class generates different ducks and displays the information to the console.
 *
 */
public class DuckGenerator {

    private final String[] DUCK_NAMES = {"Sam", "Mary", "George", "David", "Helen",
            "Eve", "Mike", "Kate", "Jessica", "Peter"};

    private Duck[] ducks;


    // This is the main method. Do not change this!
    public static void main(String[] args) {
        DuckGenerator duckGenerator = new DuckGenerator();
        duckGenerator.start();
    }

    // This is your starting method. Do not change this!
    public void start() {


        ducks = generateDucks();

        System.out.println("Greetings from Ducks");
        System.out.println("====================");

        printDuckGreetings();

        System.out.println("====================");

        System.out.println("We are plastic ducks!");
        System.out.println("---------------------");

        printPlasticDucks();
        System.out.println("--------------------");
    }

    public Duck[] generateDucks(){
        ducks = new Duck[10];
        for (int i = 0; i < ducks.length; i++) {
            int type = (int)(Math.random()*3 + 1);
            Duck next = null;
            switch (type){
                case 1: next = new RedHeadDuck(DUCK_NAMES[i]);
                break;
                case 2: next = new MallardDuck(DUCK_NAMES[i]);
                break;
                case 3: next = new RubberDuck(DUCK_NAMES[i]);
                break;
            }
            ducks[i] = next;
        }

        return ducks;
    }

    public void printDuckGreetings(){
        for (int i = 0; i < ducks.length; i++) {
            System.out.println(ducks[i].getDuckGreetings() +" "+ ducks[i].doFly());
        }
    }

    public void printPlasticDucks(){
        for (Duck duck : ducks) {
            if (duck instanceof RubberDuck){
                System.out.println(duck.getDuckGreetings());
                ((RubberDuck) duck).setMaterial("plastic");
            }
        }
    }
}
