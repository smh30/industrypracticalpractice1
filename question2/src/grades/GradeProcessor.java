package grades;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * This class imports the student grades from a file and calculates the final grades for students.
 * 
 * @author Write your UPI here
 *
 */
public class GradeProcessor {

	// TODO step c) i. Modify this method so that it handles the exception appropriately.
	private static double convertMarks(String marks) throws InvalidDataFormatException {
		if (marks == ""){
			throw new InvalidDataFormatException("Marks cannot be empty");
		}
		try {
			return Double.parseDouble(marks);
		} catch (NumberFormatException e){
			throw new InvalidDataFormatException("Marks could not be converted");
		}
	}


	private static StudentGrade processStudentGrade(String[] studentGrade) {
		StudentGrade sg = null;

		int id = Integer.parseInt(studentGrade[0]);
		String surname = studentGrade[1];
		String firstname = studentGrade[2];

		sg = new StudentGrade(id, surname, firstname);

		try {
			sg.lab1 = convertMarks(studentGrade[3]);
			sg.lab2 = convertMarks(studentGrade[4]);
			sg.lab3 = convertMarks(studentGrade[5]);
			sg.test = convertMarks(studentGrade[6]);
			sg.exam = convertMarks(studentGrade[7]);

			sg.setHonours(determineHonours(sg.getFinalMarks()));


		} catch (InvalidDataFormatException e){
			sg.setHonours(HonourClass.NA);
		}
		return sg;
	}


	private static HonourClass determineHonours(double finalMarks) {
		if (finalMarks >= 90){
			return HonourClass.FIRST;
		} else if (finalMarks >= 80){
			return HonourClass.SECOND_FIRST;
		} else if (finalMarks >=65){
			return HonourClass.SECOND_SECOND;
		}
		return HonourClass.NA;
	}
		
	// TODO step d) Complete this method so that it returns a set of StudentGrade objects
	// after importing and processing each student grade entry from a file.
	public static Set<StudentGrade> importStudentGrades(String fileName) {
		Set<StudentGrade> allStudentGrades = new HashSet<>();

		try(BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))){
			// eat the first two lines...
			br.readLine();
			br.readLine();

			String line = "";
			String[] splitLine;
			while ((line = br.readLine()) != null){
				splitLine = line.split(",");
				StudentGrade sg = processStudentGrade(splitLine);
				if (sg == null){
					continue;
				} else {
					allStudentGrades.add(sg);
				}
			}

		} catch (IOException e){
			System.out.println("An IO exception with the reader: " + e);
		}

		return allStudentGrades;
	}

}
