package grades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class provides the summary of grades for a course.
 * 
 * @author Yu-Cheng Tu
 *
 */
public class CourseSummary {
	// Stores the student grades after imported from a file
	private List<StudentGrade> grades;
	
	/**
	 * Constructor.
	 * 
	 * @param inputFileName The file name for importing the student grades
	 */
	public CourseSummary(String inputFileName) {
		grades = new ArrayList<StudentGrade>(); 
		grades.addAll(GradeProcessor.importStudentGrades(inputFileName));
	}
	
	/**
	 * This method prints the average final marks for the course.
	 */
	public void printStudentAverage() {
		double totalScore = 0;
		for (StudentGrade s: grades) {
			totalScore += s.getFinalMarks();
		}
		System.out.printf("The average for the class is %.2f%n", totalScore/grades.size());
	}
	
	/**
	 * This method prints the number of people with the given honours.
	 * 
	 * @param honours Honours for the course
	 */
	public void printCountByHonourClass(HonourClass honours) {
		int count = 0;
		for (StudentGrade s: grades) {
			if (s.getHonours().equals(honours)) {
				count++;
			}
		}
		String honourStr = getHonourString(honours);
		System.out.println("The number of people who have " + honourStr + " is: " + count);
	}
	
	// Helper method for converting each constant in HonourClass to an appropriate String
	private String getHonourString(HonourClass honours) {
		switch (honours) {
			case FIRST:
				return "first class honours";
			case SECOND_FIRST:
				return "second class first division honours";
			case SECOND_SECOND:
				return "second class second division honours";
			default:
				return "no honours";
		}
	}
		
	// TODO step e)
	public void printFirst15InOrder() {
		Comparator<StudentGrade> studentGradeComparator = new Comparator<StudentGrade>() {
			@Override
			public int compare(StudentGrade o1, StudentGrade o2) {
				return o1.getSurname().compareTo(o2.getSurname());
			}
		};
		Collections.sort(grades, studentGradeComparator);
		for (int i = 0; i < 15; i++) {
			String surname = grades.get(i).getSurname();
			Double marks = grades.get(i).getFinalMarks();
			String honours = getHonourString(grades.get(i).getHonours());

			//System.out.println(surname + ", " + marks + ", " + honours);

			System.out.printf("%s, %.2f, %s\n", surname, marks, honours);
		}

	}
	
}
