package spellchecker;

import java.util.*;

/**
 * This class is the simple spell checker, which uses a dictionary to check any words passed in from the constructor.
 * 
 * @author Write your UPI here
 *
 */
public class SimpleSpellChecker {

	private Map<String, Integer> userWords; // the words from a user 
	private IDictionary dictionary; // the dictionary to use for spelling check


	public SimpleSpellChecker(IDictionary dictionary, String wordsToCheck) {
		this.dictionary = dictionary;
		userWords = new TreeMap<>();
		String[] splitwords = wordsToCheck.split("[\\s\\W]+");
		for (int i = 0; i < splitwords.length; i++) {
			if (!userWords.containsKey(splitwords[i])) {
				userWords.put(splitwords[i], 1);
			} else {
				userWords.put(splitwords[i], userWords.get(splitwords[i])+1);
			}
		}
	}
	

	public List<String> getMisspelledWords() {
		List<String> misspelledWords = new ArrayList<String>();
		for (String word : userWords.keySet()){
			if (!Character.isDigit(word.charAt(0)) && !dictionary.isSpellingCorrect(word)){
				misspelledWords.add(word);
			}
		}
		return misspelledWords;
	}
	

	public void printUniqueWords() {
		for (String key : userWords.keySet()){
			System.out.println(key);
		}
		
	}
	

	public int getFrequencyOfWord(String word) {
		if (userWords.containsKey(word)){
			return userWords.get(word);
		}
		return 0;
	}
}
